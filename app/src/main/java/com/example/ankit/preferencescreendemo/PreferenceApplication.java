package com.example.ankit.preferencescreendemo;

import android.app.Application;
import android.preference.PreferenceManager;

public class PreferenceApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }
}
