package com.example.ankit.preferencescreendemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.widget.BaseAdapter;

import java.util.HashSet;

public class SettingsFragment
        extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {

    private static final String KEY_PREF_SYNC = "pref_sync";
    private static final String KEY_PREF_WIFI = "pref_wifi";
    private static final String KEY_PREF_EDIT_TEXT = "pref_edit_text";
    private static final String KEY_PREF_LIST = "pref_gender";
    private static final String KEY_PREF_MULTI_LIST = "pref_language";
    private static final String KEY_PREF_RINGTONE = "pref_ringtone";

    private static final String KEY_PREF_SCREEN_WIFI = "pref_screen_wifi";

    private SharedPreferences mSharedPreferences;

    public static SettingsFragment getInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        mSharedPreferences = getPreferenceScreen().getSharedPreferences();

        findPreference(KEY_PREF_SYNC).setSummary(mSharedPreferences.getBoolean(KEY_PREF_SYNC, false) ? "On" : "Off");
        findPreference(KEY_PREF_SCREEN_WIFI).setSummary(mSharedPreferences.getBoolean(KEY_PREF_WIFI, false) ? "On" : "Off");
        findPreference(KEY_PREF_EDIT_TEXT).setSummary(mSharedPreferences.getString(KEY_PREF_EDIT_TEXT, "NONE"));
        findPreference(KEY_PREF_LIST).setSummary(mSharedPreferences.getString(KEY_PREF_LIST, "male"));
        findPreference(KEY_PREF_MULTI_LIST).setSummary(mSharedPreferences.getStringSet(KEY_PREF_MULTI_LIST, new HashSet<String>()).toString());

        updateRingtoneSummary((RingtonePreference) findPreference(KEY_PREF_RINGTONE),
                Uri.parse(mSharedPreferences.getString(KEY_PREF_RINGTONE, "Silent")));
    }


    @Override
    public void onResume() {
        super.onResume();
        alarm(mSharedPreferences);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        // A patch to overcome OnSharedPreferenceChange not being called by RingtonePreference bug
        //NOTE: http://stackoverflow.com/questions/6725105/ringtonepreference-not-firing-onsharedpreferencechanged
        RingtonePreference pref = (RingtonePreference) findPreference(KEY_PREF_RINGTONE);
        pref.setOnPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case KEY_PREF_SYNC:
                Preference syncPreference = findPreference(key);
                // Set summary to be the user-description for the selected value
                syncPreference.setSummary(sharedPreferences.getBoolean(key, false) ? "On" : "Off");
                break;

            case KEY_PREF_WIFI:
                PreferenceScreen wifiPreferenceScreen = (PreferenceScreen) findPreference(KEY_PREF_SCREEN_WIFI);
                // Set summary to be the user-description for the selected value
                wifiPreferenceScreen.setSummary(sharedPreferences.getBoolean(key, false) ? "On" : "Off");
                //get the listView adapter and call notifyDataSetChanged
                //This is necessary to reflect change after coming back from sub-pref screen
                ((BaseAdapter) getPreferenceScreen().getRootAdapter()).notifyDataSetChanged();
                break;

            case KEY_PREF_EDIT_TEXT:
                Preference editTextPreference = findPreference(key);
                // Set summary to be the user-description for the selected value
                editTextPreference.setSummary(sharedPreferences.getString(key, "NONE"));
                break;

            case KEY_PREF_LIST:
                Preference listPreference = findPreference(key);
                listPreference.setSummary(sharedPreferences.getString(key, "gender"));
                break;

            case KEY_PREF_MULTI_LIST:
                Preference multiListPreference = findPreference(key);
                multiListPreference.setSummary(sharedPreferences.getStringSet(key, new HashSet<String>()).toString());
                break;
        }
    }

    //Notice that unlike onSharedPreferenceChanged, onPreferenceChange is called before the preference is updated,
    //so you must use the newValue parameter to get the selected data instead of getting it from the preference.
    //Then, set the listener on OnResume
    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        updateRingtoneSummary((RingtonePreference) preference, Uri.parse((String) newValue));
        return true;
    }

    private void updateRingtoneSummary(RingtonePreference preference, Uri ringtoneUri) {
        Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), ringtoneUri);
        if (ringtone != null)
            preference.setSummary(ringtone.getTitle(getActivity()));
        else
            preference.setSummary("Silent");
    }


    private void alarm(SharedPreferences sharedPreferences) {
        String alarms = sharedPreferences.getString(KEY_PREF_RINGTONE, "default ringtone");
        Uri uri = Uri.parse(alarms);
        playSound(getActivity(), uri);

        //call mMediaPlayer.stop(); when you want the sound to stop
    }

    private void playSound(Context context, Uri alert) {
        MediaPlayer mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }

            Thread.sleep(3000, 100);

            mMediaPlayer.stop();
        } catch (Exception e) {
            System.out.println("OOPS");
        }
    }
}
