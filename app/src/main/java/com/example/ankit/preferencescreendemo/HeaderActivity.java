package com.example.ankit.preferencescreendemo;

import android.preference.*;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

public class HeaderActivity extends android.preference.PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //Note: It's necessary to override this function to avoid
    //this error: Subclasses of PreferenceActivity must override isValidFragment(String) to verify that the Fragment class is valid!
    //http://stackoverflow.com/questions/19973034/isvalidfragment-android-api-19
    @Override
    protected boolean isValidFragment(String fragmentName) {
        return SettingsFragment.class.getName().equals(fragmentName);
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preference_headers, target);
    }
}
